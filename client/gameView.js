/*global Meteor, Template, _, jQuery, draggable, Suitup, Window */
(function (Meteor, Template, Suitup, draggable, $, win) {
	'use strict';
	var cardDragHandlers,
		suits = ["club", "diamond", "spade", "heart"],
		userDragHandlers,
		games = Suitup.games,
		cards = Suitup.cards,
		gameUsers = Suitup.gameUsers;


	cardDragHandlers = draggable.createHandlers({
		onDragStart: function (e, ctx) {
			var target = $(e.target),
				card = ctx.target;
			cards.select(card);
			if (target.hasClass('card-bt-show')) {
				cards.setState('V');
			} else if (target.hasClass('card-bt-peek')) {
				cards.setState('P');
			} else if (target.hasClass('card-bt-hide')) {
				cards.setState('H');
			} else if (target.hasClass('group')) {
				cards.toggleJoin();
			} else if (target.hasClass('shuffle')) {
				cards.shuffle();
			} else if (target.hasClass('sort')) {
				cards.sort();
			} else if (target.hasClass('collect')) {
				cards.collect();
			} else if (target.hasClass('spread-left')) {
				cards.spreadLeft();
			} else if (target.hasClass('spread-down')) {
				cards.spreadDown();
			}
		},
		onDragging: function (e, ctx) {
			cards.incrementPosition(ctx.dx, ctx.dy);
		},
		onDragEnd: function (e, ctx) {
			cards.endDragging();
		}
	});

	userDragHandlers = draggable.createHandlers({
		onDragStart: function (e, ctx) {
			gameUsers.select(ctx.target._id);
		},
		onDragging: function (e, ctx) {
			gameUsers.incrementPosition(ctx.dx, ctx.dy);
		},
		onDragEnd: function (e, ctx) {

		}
	});

	function getStateOnClass(qstate) {
		return function () {
			var card = cards.getSelected();
			return (card && card.state === qstate) ? 'card-state-on' : '';
		};
	}

	Template.board.cards = cards.find;

	Template.board.cardVisible = getStateOnClass('V');
	Template.board.cardPrivate = getStateOnClass('P');
	Template.board.cardHidden = getStateOnClass('H');
	Template.card.groupStateClass = function () {
		return cards.isSelectedAndJoined(this._id) ? 'group-on' : 'group-off';
	};
	Template.card.groupButtonText = function () {
		return cards.isSelectedAndJoined(this._id) ? 'Release' : 'Join';
	};

	Template.board.users = function () {
		var cursor = games.findGameUsers();
		cursor.observe({
			removed: function (gameUser) {
				console.log(gameUser);
				if (gameUser.userId === Meteor.userId()) {
					Meteor.Router.to('/');
				}
			}
		});
		return cursor;
	};

	Template.card.label = function () {
		if (this.state === 'V' || (this.state === 'P' && this.userId === Meteor.userId())) {
			return "card-visible " + suits[this.suit] + "-" + this.value;
		}
		return '';
	};
	Template.card.showClass = function () {
		return this.state === 'V' ? 'hidden' : '';
	};
	Template.card.peekClass = function () {
		return this.state === 'P' ? 'hidden' : '';
	};
	Template.card.hideClass = function () {
		return this.state === 'H' ? 'hidden' : '';
	};

	Template.card.cardStyle = function () {
		var style = '';
		if (cards.isSelected(this._id)) {
			style = 'card-selected';
		}
		if (this.dragging) {
			style += ' card-dragging';
		}
		if (this.state === 'P') {
			style += ' card-private';
		}
		return style;
	};

	Template.card.cardColor = function () {
		return this.state === 'P' ? this.color : '#666';
	};

	Template.card.events({
		'mousedown .card': cardDragHandlers.onDragStart
	});

	function nodeId(node) {
		return node.id;
	}

	Template.card.preserve({
		'.card': nodeId,
		'.state-menu': nodeId,
		'.group-menu': nodeId
	});

	Template.user.preserve({
		'.user': nodeId,
		'img': nodeId
	});

	Template.board.events({
		'mousemove .board': function (e) {
			cardDragHandlers.onDragging(e);
			userDragHandlers.onDragging(e);
		},
		'mouseup .board': function (e) {
			cardDragHandlers.onDragEnd(e);
			userDragHandlers.onDragEnd(e);
		}
	});

	Template.user.events({
		'mousedown .user': userDragHandlers.onDragStart,
		'click .game-quit-bt': function () {
			if (win.confirm('Are you sure?')) {
				gameUsers.quit(this);
			}
		}
	});

	Template.user.pic = function () {
		return this.picture || "/img/user.png";
	};
	Template.user.quitClass = function () {
		return gameUsers.canQuit(this) ? '' : 'hidden';
	};
}(Meteor, Template, Suitup, draggable, jQuery, window));
