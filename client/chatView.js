/*global Meteor, Session, Template, Suitup, jQuery */
(function (Meteor, Session, Template, Suitup, $) {
	'use strict';
	var chat = Suitup.gameChat;
	Session.setDefault('showChat', true);

	Template.chat.chats = function () {
		var cursor = chat.find();
		cursor.observe({
			added: function (chat) {
				var chatList = $('.chat-list-list');
				$('.chat-list').animate({scrollTop: chatList.height()}, '50');
			}
		});
		return cursor;
	};

	Template.chat.chatStateClass = function () {
		return Session.get('showChat') ? 'chat-open' : 'chat-closed';
	};

	Template.chat.events({
		'keydown .chat-prompt': function (e) {
			if (e.which === 13 && e.target.value.trim()) {
				chat.create(e.target.value.trim());
				e.target.value = "";
			}
		},
		'click .chat-toggle-show': function () {
			var showChat = Session.get('showChat');
			Session.set('showChat', !showChat);
		}
	});

	Template.chat.preserve({
		'.chat-block': function (node) {return node.id; }
	});
}(Meteor, Session, Template, Suitup, jQuery));