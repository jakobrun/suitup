/*global Meteor, Session, Suitup */
(function (Meteor, Session, Suitup) {
	'use strict';
	var m = {};
	Suitup.gameUsers = m;

	m.select = function (gameUserId) {
		Session.set('currentGameUser', gameUserId);
	};

	m.incrementPosition = function (dx, dy) {
		Suitup.GameUsers.update(Session.get('currentGameUser'), {$inc: {left: dx, top: dy}});
	};

	m.user = function () {
		return Suitup.GameUsers.findOne({gameId: Session.get('currentGame'), userId: Meteor.userId()});

	};
	m.quit = function (gameUser) {
		Suitup.GameUsers.remove({_id: gameUser._id});
	};
	m.canQuit = function (gameUser) {
		var userid = Meteor.userId(),
			game = Suitup.Games.findOne({_id: Session.get('currentGame')});
		return ((userid === gameUser.userId) ||
			(game.user._id === userid));
	};
}(Meteor, Session, Suitup));