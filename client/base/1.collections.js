/*global Meteor */
var Suitup = {}, Games, Cards, GameUsers, GameChat;
Meteor.subscribe('allGames');
Meteor.subscribe('allCards');
Meteor.subscribe('allGameUsers');
Meteor.subscribe('allGameChat');

Suitup.Games = Games = new Meteor.Collection("games");
Suitup.Cards = Cards = new Meteor.Collection("cards");
Suitup.GameUsers = GameUsers = new Meteor.Collection("gameUsers");
Suitup.GameChat = GameChat = new Meteor.Collection("gameChat");