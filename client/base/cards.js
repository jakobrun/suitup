/*global Meteor, Session, Suitup, _ */
(function (Meteor, Session, Suitup, _) {
	'use strict';
	var m = {},
		Cards = Suitup.Cards,
		Games = Suitup.Games;
	Suitup.cards = m;

	function firstArg(arg) {
		return arg;
	}

	m.select = function (card) {
		var id = card._id,
			maxZ = _.max(m.find().fetch(), function (card) {return card.z; });
		card.dragging = true;
		Cards.update(card._id, {$set: { z: (maxZ.z + 1)}});
		if (!m.isSelected(id)) {
			Session.set('selectedCardGroupIndex', undefined);
		}
		Session.set('selectedCard', id);
	};

	m.find = function () {
		var gameId = Session.get('currentGame');
		return Cards.find({gameId: gameId});
	};

	m.getSelected = function () {
		return Cards.findOne({_id: Session.get('selectedCard')});
	};

	m.isSelectedAndJoined = function (cardId) {
		return Session.get('selectedCardGroupIndex') && m.isSelected(cardId);
	};

	m.isSelected = function (cardId) {
		var cardGroupIndex = Session.get('selectedCardGroupIndex');
		return cardId === Session.get('selectedCard') || (cardGroupIndex && cardGroupIndex[cardId]);
	};

	m.getAllSelected = function () {
		return _.values(Session.get('selectedCardGroupIndex'));
	};

	m.eachSelected = function (f) {
		var id = Session.get('selectedCard'),
			cardGroupIndex = Session.get('selectedCardGroupIndex');
		if (cardGroupIndex) {
			_.keys(cardGroupIndex).forEach(f);
		} else {
			f(id);
		}
	};

	m.incrementPosition = function (dx, dy) {
		m.eachSelected(function (cardId) {
			Cards.update(cardId, {$inc: {x: dx, y: dy}, $set: {dragging: true}});
		});
	};

	m.endDragging = function () {
		m.eachSelected(function (cardId) {
			Cards.update(cardId, {$set: {dragging: false}});
		});
		if (Session.get('selectedCardGroupIndex')) {
			Session.set('selectedCardGroupIndex', m.selectGroup());
		}
	};

	m.getGroup = function (card, group, cards) {
		var list = cards.filter(function (c) {return c._id !== card._id; }),
			groups,
			cardsNotInGroup;
		group[card._id] = card;

		groups = _.groupBy(list, function (c) {
			return Math.abs(card.x - c.x) < 81 && Math.abs(card.y - c.y) < 123;
		});
		if (groups[true]) {
			if (groups[false]) {
				groups[true].forEach(function (c) {
					m.getGroup(c, group, groups[false]);
				});
			} else {
				groups[true].forEach(function (c) {
					group[c._id] = c;
				});
			}
		}
		return group;
	};

	m.setState = function (newState) {
		var cardGroupIndex = Session.get('selectedCardGroupIndex'),
			color = Suitup.gameUsers.user().color,
			updateState = function (cardId) {
				Cards.update(cardId, {$set: {state: newState, userId: Meteor.userId(), color: color}});
			};
		if (cardGroupIndex) {
			_.keys(cardGroupIndex).forEach(updateState);
		} else {
			updateState(Session.get('selectedCard'));
		}
	};

	m.selectGroup = function () {
		var card = m.getSelected(),
			groupIndex = m.getGroup(card, {}, m.find().fetch());
		_.values(groupIndex).forEach(function (card) {
			Cards.update(card._id, {$set: {userId: Meteor.userId()}});
		});
		return groupIndex;
	};

	m.ensureCardGroupSelected = function () {
		var groupIndex = Session.get('selectedCardGroupIndex'),
			card;
		if (!groupIndex) {
			Session.set('selectedCardGroupIndex', m.selectGroup());
			return false;
		}
		return true;
	};

	m.toggleJoin = function () {
		var groupIndex = Session.get('selectedCardGroupIndex');
		if (groupIndex) {
			groupIndex = undefined;
		} else {
			groupIndex = m.selectGroup();
		}
		Session.set('selectedCardGroupIndex', groupIndex);
	};

	m.shuffle = function () {
		var cards = m.getAllSelected(),
			shuffeledCards = _.shuffle(cards);
		shuffeledCards.forEach(function (card, index) {
			Cards.update(card._id, {$set: {z: index}});
		});
	};

	m.sort = function () {
		var cards = m.getAllSelected(),
			xValues = _(cards).chain().pluck('x').sortBy(firstArg).value(),
			yValues = _(cards).chain().pluck('y').sortBy(firstArg).value(),
			sortedCards = _.sortBy(cards, function (card) {
				return ((card.suit * 100) + card.value);
			});
		sortedCards.forEach(function (card, index) {
			Cards.update(card._id, {$set: {z: index, x: xValues[index], y: yValues[index]}});
		});
		Session.set('selectedCardGroupIndex', m.selectGroup());
	};

	m.collect = function () {
		var selectedCard = m.getSelected(),
			cards = m.getAllSelected(),
			factor = cards.length / 5;

		_.sortBy(cards, function (card) {return -card.z; }).forEach(function (card, index) {
			card.x = (selectedCard.x + Math.floor(index / factor));
			card.y = selectedCard.y;
			Cards.update(card._id, {$set: {x: card.x, y: card.y}});
		});
		Session.set('selectedCardGroupIndex', m.selectGroup());
	};

	m.spreadLeft = function () {
		var selectedCard = m.getSelected(),
			cards = _(m.getAllSelected()).chain(),
			selX = cards.pluck('x').min().value(),
			selY = selectedCard.y,
			space = 50;
		cards.sortBy(function (card) {return card.z; }).value().forEach(function (card, index) {
			card.x = (selX + (index * space));
			card.y = selY;
			Cards.update(card._id, {$set: {x: card.x, y: card.y}});
		});
		Session.set('selectedCardGroupIndex', m.selectGroup());
	};

	m.spreadDown = function () {
		var selectedCard = m.getSelected(),
			cards = _(m.getAllSelected()).chain(),
			selX = selectedCard.x,
			selY = cards.pluck('y').min().value(),
			space = 80;
		cards.sortBy(function (card) {return card.z; }).value().forEach(function (card, index) {
			card.x = selX;
			card.y = (selY + (index * space));
			Cards.update(card._id, {$set: {x: card.x, y: card.y}});
		});
		Session.set('selectedCardGroupIndex', m.selectGroup());
	};

}(Meteor, Session, Suitup, _));