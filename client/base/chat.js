/*global Meteor, Session, Suitup */
(function (Meteor, Session, Suitup) {
	'use strict';
	var m = {};
	m.find = function () {
		var gameId = Session.get('currentGame');
		return Suitup.GameChat.find({gameId: gameId});
	};
	m.create = function (text) {
		var gameId = Session.get('currentGame'),
			gameUser = Suitup.gameUsers.user(),
			chatUser = {userId: gameUser.userId, name: gameUser.name, color: gameUser.color};
		Suitup.GameChat.insert({gameId: gameId, user: chatUser, text: text, createTime: new Date()});
	};
	Suitup.gameChat = m;
}(Meteor, Session, Suitup));