/*global Meteor, Session, Suitup, _, Q */
(function (Meteor, Session, Suitup, _, Q) {
	'use strict';
	var m = {},
		userColors = ['green', 'red', 'blue', 'orange'];
	Suitup.games = m;

	//Find free top position for gameuser
	function freeTopPos(gameUsers) {
		var top,
			i,
			containsTop = function (u) {
				return (u.top <= this.top && this.top < (u.top + 130));
			};

		for (i = 0; i < 100; i += 1) {
			top = i * 130 + 5;
			if (!_.some(gameUsers, containsTop, {top: top})) {
				break;
			}
		}
		return top;
	}

	m.create = function (name, cb) {
		var user = Meteor.user(),
			newGame = {name: name, createTime: new Date(), user: {_id: user._id, name: user.profile.name}};

		Suitup.Games.insert(newGame, function (err, gameId) {
			var suitIndex, valueIndex, card, pos = [], i, factor = 52 / 5;
			for (i = 0; i < 52; i += 1) {
				pos.push({z: i, x: (340 - Math.floor(i / factor))});
			}
			pos = _.shuffle(pos);
			newGame._id = gameId;
			i = 0;
			for (suitIndex = 0; suitIndex < 4; suitIndex += 1) {
				for (valueIndex = 1; valueIndex <= 13; valueIndex += 1) {
					card = {suit: suitIndex,
									value: valueIndex,
									state: 'H',
									userId: '',
									gameId: gameId,
									x: pos[i].x,
									y: 200,
									z: pos[i].z
							};
					i += 1;
					Suitup.Cards.insert(card);
				}
			}
			if (cb) {
				cb(newGame);
			}
		});

	};

	function removePromise(Collection, selector) {
		var d = Q.defer();
		Collection.remove(selector, function (err) {
			if (err) {
				d.reject(new Error(err));
			} else {
				d.resolve();
			}
		});
		return d.promise;
	}

	m.remove = function (gameId) {
		var game = {gameId: gameId};
		Q.all([removePromise(Suitup.Cards, game),
			removePromise(Suitup.GameUsers, game),
			removePromise(Suitup.GameChat, game)
			])
			.then(function () {
				Suitup.Games.remove(gameId);
			});
	};

	m.select = function (gameId) {
		m.addUser(gameId, Meteor.user());
		Session.set('currentGame', gameId);
	};

	m.addUser = function (gameId, user) {
		var game,
			gameUsers;
		if (user) {
			game = Suitup.Games.findOne({_id: gameId});

			if (game) {
				gameUsers = Suitup.GameUsers.find({gameId: game._id}).fetch();
				if (gameUsers.filter(function (u) {return u.userId === user._id; }).length === 0) {

					Suitup.GameUsers.insert({userId: user._id,
									gameId: game._id,
									name: user.profile.name,
									left: 5,
									top: freeTopPos(gameUsers),
									color: userColors[gameUsers.length],
									picture: user.profile.picture});

				}
			}
		}
	};

	m.find = function () {
		return Suitup.Games.find();
	};

	m.findByName = function (name) {
		return Suitup.Games.find({name: { $regex: '.*' + name + '.*', $options: 'i'}});
	};

	m.findGameUsers = function () {
		return Suitup.GameUsers.find({gameId: Session.get('currentGame')});
	};

}(Meteor, Session, Suitup, _, Q));