var draggable = (function () {
	'use strict';

	return {
		createHandlers: function (handlers) {
			var clientX, clientY, dragging = false;
			return {
				onDragStart: function (e) {
					handlers.onDragStart(e, {target: this});
					dragging = true;
					clientX = e.clientX;
					clientY = e.clientY;
					e.preventDefault();
				},
				onDragging: function (e) {
					var x, y;
					if (!dragging) {
						return;
					}
					x = e.clientX - clientX;
					y = e.clientY - clientY;
					handlers.onDragging(e, {target: this, dx: x, dy: y});
					clientX = e.clientX;
					clientY = e.clientY;
					e.preventDefault();
				},
				onDragEnd: function (e) {
					if (!dragging) {
						return;
					}
					handlers.onDragEnd(e, {target: this});
					dragging = false;
					e.preventDefault();
				}
			};
		}
	};
}());