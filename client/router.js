/*global Meteor, Suitup */
(function (Mtr, Su) {
	'use strict';
	//Router
	Mtr.Router.add({
		'/': 'games',
		'/game/:id': function (id) {
			Su.games.select(id);
			return 'board';
		}
	});
}(Meteor, Suitup));