/*global Meteor, Template, Session, Handlebars, Suitup, moment, jQuery, setTimeout, _ */
(function (Meteor, Template, Session, Handlebars, Suitup, moment, $, setTimeout, _) {
	'use strict';
	var games = Suitup.games;
		//Helpers
	Handlebars.registerHelper('formatTimestamp', function (input) {
		if (input) {
			return moment(input).format('DD.MM.YYYY HH:mm:ss');
		}
		return '';
	});

	Session.setDefault('gameSearch', '');

	function findGames() {
		return games.findByName(Session.get('gameSearch'));
	}

	Template.games.list = findGames;

	Template.games.searchValue = function () {
		return Session.get('gameSearch');
	};

	function nodeId(node) {
		return node.id;
	}

	function closePopup() {
		$('body').removeClass('popup-active');
		$('.game-shearch').focus();
	}

	function goToGame(game) {
		Meteor.Router.to('/game/' + game._id);
	}

	function createGame(nameInp) {
		var val = nameInp.val().trim();
		if (val) {
			games.create(val, goToGame);
			nameInp.val('');
			closePopup();
		}
	}

	Template.popup.events({
		'click .popup-glass': function (e) {
			closePopup();
		},
		'keydown .new-game-name': function (e) {
			if (e.which === 13 && e.target.value.trim()) {
				createGame($('.new-game-name'));
			}
		},
		'click .creategame-bt': function () {
			createGame($('.new-game-name'));
		},
		'click .new-game-cancel-bt': function () {
			closePopup();
		}
	});

	Template.games.events({
		'click .new-game': function (e) {
			$('body').toggleClass('popup-active');
			setTimeout(function () {
				$('.new-game-name').focus();
			}, 300);
		},
		'keydown .game-shearch': function (e) {
			var row = Session.get('selectedGameRow'), index, games, next;
			if (e.which === 40 || e.which === 38) { //Down or up
				games = findGames().fetch();
				if (games.length === 0) {
					return;
				}
				if (row) {
					index = _(games).chain().map(function (game) {
						return game._id;
					}).indexOf(row._id).value() + ((e.which === 40) ? 1 : -1);
					next = games[index % games.length];
				} else {
					next = games[0];
				}
				e.preventDefault();
				Session.set('selectedGameRow', next);
			} else if (e.which === 13 && row) {
				goToGame(row);
			}
		},
		'keyup .game-shearch': function () {
			Session.set('gameSearch', $('.game-shearch').val());
		},
		'click .game-remove': function (e) {
			games.remove(this._id);
			e.stopPropagation();
		}
	});

	Template.game.gameClass = function () {
		var row = Session.get('selectedGameRow');
		return (row && this._id === row._id) ? 'game-selected' : '';
	};

	Template.game.events({
		'click .game': function (e) {
			goToGame(this);
		}
	});

	Template.games.preserve({
		'.game-shearch': nodeId
	});

}(Meteor, Template, Session, Handlebars, Suitup, moment, jQuery, setTimeout, _));