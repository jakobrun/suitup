/*global Meteor, Accounts, _ */
(function (Meteor, Accounts) {
	'use strict';
	var Games = new Meteor.Collection("games"),
		Cards = new Meteor.Collection("cards"),
		GameUsers = new Meteor.Collection("gameUsers"),
		GameChat = new Meteor.Collection("gameChat"),
		allowOptions = {
			update: function (userId, docs, fieldNames, modifier) {
				return true;
			},
			insert: function (userId, doc) {
				return userId;
			},
			remove: function (userId, docs) {
				return (_(docs).chain().pluck('userId').all(function (objUserId) {
					return objUserId === userId;
				}).value() || _(docs).chain().pluck('gameId').uniq().all(function (gameId) {
					var game = Games.findOne({_id: gameId});
					return (game && game.user && game.user._id === userId);
				}).value());
			}
		};

	//Publish
	Meteor.publish('allGames', function () {
		if (this.userId) {
			return Games.find();
		}
	});

	Meteor.publish('allCards', function () {
		if (this.userId) {
			return Cards.find();
		}
	});

	Meteor.publish('allGameUsers', function () {
		if (this.userId) {
			return GameUsers.find();
		}
	});

	Meteor.publish('allGameChat', function () {
		if (this.userId) {
			return GameChat.find();
		}
	});

	function allUserDocs(userId, docs) {
		return _.all(docs, function (doc) {
			return (!doc.user || userId === doc.user._id);
		});
	}

	//Allow
	Games.allow({
		update: allUserDocs,
		insert: function (userId, doc) {
			return userId;
		},
		remove: allUserDocs
	});
	Cards.allow(allowOptions);
	GameUsers.allow(allowOptions);
	GameChat.allow(allowOptions);


	// store picture url from google
	Accounts.onCreateUser(function (options, user) {
		if (user.services && user.services.google) {
			user.profile = {name: user.services.google.name,
				picture: user.services.google.picture};

		}
		return user;
	});
}(Meteor, Accounts));